package org.example;

public class Cat extends Animal{
    public Cat(String name, boolean isAngry) {
        super(name, isAngry);
    }

    //    private String name;
//    private boolean isAngry;
//-------- Конструкторы ---------------------------
//    public Cat(String name) {
//        this(name,true); //тоже самое что и ниже
//    }

 //   public Cat(String name, boolean isAngry) { //новый конструктор (замена старого выше)
 //       this.name = name;
 //       this.isAngry = isAngry;
 //   }
//------------ Методы ---------------------------------------------------
//    public void printName(){
//        String name = "Барсик";
        //System.out.println("Имя Кота - 1: " + name);
 //       System.out.println("Имя Кота - 2: " + getName());
 //   }
 public void say(){
     System.out.println("Мяу!!!");
 }

 //    public void sayMeow(int n){
 //       for (int i = 0; i < n; i++){
 //           System.out.println("Мяу!!!");
 //       }
 //   }
    //------------ Гетторы и Сетторы ----------------------------------------
 /*   public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }

  */
    //---------------------------------------------------------------------
}
