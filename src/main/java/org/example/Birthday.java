package org.example;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Birthday {
    static int getAge(int year, int month, int date){
        LocalDate dateRogd = LocalDate.of(year, month, date);
        LocalDate tecDate = LocalDate.now();
        int daysProhlo = (int) ChronoUnit.DAYS.between(dateRogd, tecDate);
        return daysProhlo;
    }
    static LocalDate nextBirthday(int year, int month, int date){
        LocalDate dateRogd = LocalDate.of(year, month, date);
        LocalDate tecDate = LocalDate.now();
        int daysProhlo = (int) ChronoUnit.DAYS.between(dateRogd, tecDate);
    //    int prPer = (daysProhlo / 1000 + 1) * 1000;
        LocalDate nextDr = dateRogd.plus((daysProhlo / 1000 + 1) * 1000, ChronoUnit.DAYS);
        return nextDr;
    }
}