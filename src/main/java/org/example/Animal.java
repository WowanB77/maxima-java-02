package org.example;

public class Animal {
    private String name;
    private boolean isAngry;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }

    public Animal(String name, boolean isAngry) {
        this.name = name;
        this.isAngry = isAngry;
    }
    public void say(){
        System.out.println("Привет!!!");
    }
}

