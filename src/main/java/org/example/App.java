package org.example; // пакет

public class App     // тело класса
{
    public static void  main( String[] args ) {
//-------------------------- Домашка к уроку 9 ------------------------------------------
/*        System.out.println("Прошло дней со дня моего рождения: " + Birthday.getAge(1977, 9, 8));
        System.out.println("День рождения будем проздновать " + Birthday.nextBirthday(1977, 9, 8));
    }

 */
//-------------------------- Домашка к уроку 6 ------------------------------------------
        City gorod = new City("Камышлов ", 2320);
//        City gorod = new City("Камышлов ", 2320,false,false);
        Transport transport = new Transport("DAF", 22000, 90, 55);
        //    City tokio = new City ("TOKIO", 9100 ,true, true);
        Ship ship = new Ship("Сухогруз", 3800, 10, 40);
        Plane plane = new Plane("ИЛ-76", 1580, 810, 80);
        //    Truck truck = new Truck("Камаз", 2320,90, 55);

        System.out.println("До " + gorod.getName() + " - " + gorod.getDistanceKm() + " км.");
        System.out.println("Стоимость перевозки " + transport.getName() + ": " + transport.getPrice(gorod));
        System.out.println("Стоимость перевозки " + ship.getName() + ": " + ship.getPrice(gorod));
        System.out.println("Стоимость перевозки " + plane.getName()+ ": " + plane.getPrice(gorod));
    }
//-------------------------- Домашка к уроку 5 ------------------------------------------
 /*       City gorod = new City("Камышлов ", 2320 );
        Transport transport = new Transport("DAF", 22000, 90, 55);
        System.out.println("До " + gorod.getName() + " - " + gorod.getDistanceKm() + " км.");
        System.out.println("Стоимость: " + transport.getPrice(gorod));

  */



        //------------------- Урок 5 - 6 -------------------------------------------
 /*       final String MAX_DAYS = "11111"; // запрещает менять переменную (константа)

        final Cat murzik = new Cat("Мурзик", false); // описали объект murzik, далее работаем с ним
        Cat barsik = new Cat("Барсик", false);
        Dog sharick = new Dog("Шарик", true);
        Horse burushka = new Horse("Бурушка", false);

    //    murzik.setName("Барсик");
        barsik = murzik;
        System.out.println(barsik.getName());
        //murzik.setName("Мурзик");
        //System.out.println(murzik.name);
    //    murzik.printName(); // обращаемся к методу printName объекта murzik
//        System.out.println(murzik.getName());
//        System.out.println(murzik.isAngry());

    //    Cat barsik = new Cat("Барсик"); // не статический метод
    //    barsik.setName();
    //    barsik.printName();

     //   Cat.sayMeow(3); // статический метод
//        System.out.println(murzik instanceof Cat);
//        System.out.println(murzik instanceof Animal);

//        System.out.println(sharick instanceof Dog);
//        System.out.println(sharick instanceof Animal);

//        Animal animal = murzik;
//        System.out.println(animal.getName());

//        Cat barsik = (Cat) animal;
        murzik.say();
        sharick.say();
        burushka.say();

    }

  */
}
